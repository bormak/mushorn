
load("fx:base.js")
load("fx:controls.js")
load("fx:graphics.js")
load("fx:media.js")

load("./bower_components/lodash/lodash.min.js")
load("utils.js")



var mh = {
  width: 1024,
  height: 572,
  re: /\.mp3$|\.mp4a$/,
  dirfile:  '.mhdirs',
  lastfile: '.mhlast',
  root: new TabPane(),
  tabs: {
    play: new Tab('Player'),
    art: new Tab('Artists')
  },
  grids: {
    play: new VBox(10),
    art:  new ScrollPane(),
  },
  panes: {
    ctl: new HBox(10),
    trascr: new ScrollPane(),
  },
  lbl: {},
  btn: new Button(),
  tracks: [],
}


function start(stage) {

  _.each(['art', 'alb','track', 'sel'], function(key) {
    mh.lbl[key] = new Label();
    mh.lbl[key].style = "-fx-text-fill: green";
  });

  _.each(['trafl', 'alb', 'art'], function(key) {
    mh.panes[key] = new FlowPane(Orientation.HORIZONTAL);
  });

  confScroll(mh.panes.trascr, mh.panes.trafl)

  mh.btn.onAction = ctlPlay;
  mh.btn.style = "-fx-text-fill: red"

  mh.panes.ctl.children.addAll(mh.lbl.art, mh.lbl.alb, mh.lbl.track, mh.btn);

  mh.root.tabs.addAll(mh.tabs.play, mh.tabs.art);

  mh.root.onKeyReleased = function(e) {
    if (e.code == KeyCode.F1) {
      var sm = mh.root.selectionModel;;
      sm.select(1 - sm.selectedIndex);
    }
    else if (e.code == KeyCode.F2) {
      ctlPlay();
    } 
  };

  mh.root.selectionModel.select(1);
  loadLast();

  loadArtists();
  mh.tabs.play.content = mh.grids.play
  mh.tabs.art.content = mh.grids.art

  var lbl = new Label('Tracks:');
  lbl.style = "-fx-text-fill: green" // to do: define css for labels
  mh.grids.play.children.addAll(mh.panes.ctl, lbl, mh.panes.trascr,
				mh.lbl.sel, mh.panes.alb);
  
  confScroll(mh.grids.art, mh.panes.art);

  stage.scene = new Scene(mh.root, mh.width, mh.height);
  stage.title = "Mushorn";
  stage.show();
}
