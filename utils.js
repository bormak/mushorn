
function play(i) {
  if (mh.mp)
    mh.mp.stop();

  if (i != undefined)
    mh.pos = i;
  else
    mh.pos++;
  // to do: play next album if pos is over

  var track = mh.tracks[mh.pos];
  try {
    var m = new Media(new java.io.File(track).toURI().toString());
    // to do: show meta data ?
    mh.mp = new MediaPlayer(m);
  }
  catch(e) {
    print(e);
    return;
  }

  mh.mp.onEndOfMedia = play;

  mh.mp.play();
  mh.btn.text = 'Pause';
  mh.lbl.track.text = track.name;
  store();
}

function loadDir(dir) {
  _.each(list(dir).sort(), function(file) {
    if (file.isDirectory())
      loadDir(file);
    else {
      if (!file.name.match(mh.re))
    	  return;
      btn = makeButton(file, 30)
      btn.onAction = function(i) {
    	  return function() play(i);
      }(mh.tracks.length);
      // due to multi-directory albums each index can't be used

      mh.panes.trafl.children.add(btn);
      mh.tracks.push(file);
    }
  });
}



function loadTracks(alb, idx) {
  var btn;
  mh.tracks = [];
  mh.panes.trafl.children.clear();

  if (alb.isFile() && alb.name.match(mh.re)) {
    mh.tracks.push(alb);
    mh.lbl.alb.text = alb.name.replace(mh.re, '');
  }
  else {
    loadDir(alb);
    mh.lbl.alb.text = alb.name;
  }

  mh.alb = alb;
  mh.art = mh.selected;
  mh.lbl.art.text = mh.selected.name;

  if (idx == undefined)
    idx = 0;
  play(idx);
}

function loadAlbums(dir) {
  mh.root.selectionModel.select(0);
  mh.lbl.sel.text = "Albums of ${dir.name}:";
  show(list(dir).sort(), 'alb', loadTracks, 40);
  mh.selected = dir;
}


function loadArtists() {
  var dirs = read(mh.dirfile," ");
    var files = [];
    var re =
  _.each(dirs, function(dir) {
      files = files.concat(list(dir.trim()));
  });
  files = _.sortBy(files, function(file) file.name)
  show(files, 'art', loadAlbums, 25)
}

function loadLast() {
  var data = read(mh.lastfile,"\n");
  if (!data)
    return;
  mh.art = mh.selected = new java.io.File(data[0]);
  mh.alb = new java.io.File(data[1]);
  // needed to get .name
  mh.pos = parseInt(data[2]);
  loadAlbums(mh.art);
  loadTracks(mh.alb, mh.pos);
}



function show(files, pane, fun, size) {
  mh.panes[pane].children.clear();
  _.each(files, function(file) {
    btn = makeButton(file, size)
    btn.onAction = function(arg) {
      return function() fun(arg);
    }(file);
    mh.panes[pane].children.add(btn);
  });
}


function list(dir) {
  files = _.map((new java.io.File(dir)).listFiles(), function(f) f)
  if ($ENV.HOME.match('Users'))
    return _.reject(files, function (f) f.name.match('DS_Store'));
  else
    return files;
}

function makeButton(file, size) {
  btn = new Button(_.trunc(file.name.replace(mh.re,''), size));
  btn.mnemonicParsing = false;
  btn.style = "-fx-text-fill: blue"
  return btn;
}

function store() {
  var out = new java.io.PrintWriter("${$ENV.HOME}/${mh.lastfile}");
  _.each([mh.art, mh.alb, mh.pos], function(str) {
    out.println(str) // needs separate line due to spaces in names
  });
  out.close();
}

function read(file, sep) {
  var p = java.nio.file.Paths.get("${$ENV.HOME}/${file}");
  try {
    return (new java.lang.String(java.nio.file.Files.readAllBytes(p))).split(sep)
  }
  catch(e) {
    return null;
  }
}

function ctlPlay()  {
  if (!mh.mp)
    return;
  if (mh.mp.status == MediaPlayer.Status.PLAYING) {
    mh.btn.text = 'Play'
    mh.mp.pause();
  }
  else {
    mh.btn.text = 'Pause'
    mh.mp.play();
  }
}

function confScroll(scroll, content) {
  scroll.pannable = true;
  scroll.fitToWidth = true;
  scroll.content = content;
}
